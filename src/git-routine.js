const git = require('simple-git');
const responseBuilder = require('./response-builder')
const getCurrentBranchName = require('node-git-current-branch');

const onValidRequest = (repoPath, res) => {
  const simpleGit = git(repoPath)
  const branch = getCurrentBranchName(repoPath)
  simpleGit.fetch('origin', branch)
  simpleGit.reset('hard')
  simpleGit.pull('origin', branch)
  responseBuilder(res, 200, 'ok')
}

module.exports = onValidRequest;