const bodyParser = require('body-parser')
const app = require('express')();
const join = require('path').join;
const onValidRequest = require('./git-routine')
const responseBuilder = require('./response-builder')
const { isBranchValid, isRepoValid, isSigned } = require('./request-validator')
const { endpoint, port, ip, reposPath } = require('../config.json')

const jsonParser = bodyParser.json({ verify: isSigned })

app.post(endpoint, jsonParser, (req, res) => {
  const { body, authenticated } = req
  const { ref, repository: { name } } = body
  if (!authenticated)
    responseBuilder(res, 400, 'invalid secret')
  else if (!isRepoValid(name, reposPath))
    responseBuilder(res, 404, 'repo not found')
  else if (!isBranchValid(reposPath, name, ref))
    responseBuilder(res, 400, 'invalid branch')
  else { //REQ IS VALID HERE
    const repoPath = join(reposPath, name)
    onValidRequest(repoPath, res)
  }
})
//error handling
app.use((_err, _req, res, _next) => {
  responseBuilder(res, 500, 'NOT A GOGS REQUEST')
});

app.listen(port);
console.log(`Server running at ${ip}:${port}\nRemember to set params in config.js`);