const responseBuilder = (res, resCode, msg) => {
  res.writeHead(resCode, { 'Content-Type': 'text/plain' });
  res.end(msg);
}

module.exports = responseBuilder;