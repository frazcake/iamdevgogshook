const { join, parse } = require('path')
const { readdirSync } = require('fs');
const { createHmac } = require('crypto');
const getBranch = require('node-git-current-branch');
const { secret } = require('../config.json')

/* Callback function to be provided in body-parser.json().
   It set a custom boolean property on req object (authenticated) 
   that can be used inside express to check if valid or not */
const isSigned = (req, _res, body, _encoding) => {
  const reqHmac = req.get("x-gogs-signature")
  const myHmac = createHmac('sha256', secret)
    .update(body)
    .digest('hex')
  req.authenticated = reqHmac === myHmac
}

const isRepoValid = (reqRepoName, reposPath) => {
  return readdirSync(reposPath).includes(reqRepoName)
}

const isBranchValid = (reposPath, repoName, reqBranchRef) => {
  const reqBranch = parse(reqBranchRef).base
  const repoPath = join(reposPath, repoName)
  return reqBranch === getBranch(repoPath);
}

module.exports = {
  isBranchValid,
  isSigned,
  isRepoValid
}